# devoirpython



## Getting started

This script creates an hierarchical organistion of folders and files for a data projet.


## How it was built

This script uses a single python file to create and commit to a given repo the necessary folders and files for a data projet.

## How to use this script
There are 4 methods
    setDataProjecthierarchy.createSingleDir(list of folder at level 1) : create a list of directories given in the parameter as a list of strings
    setDataProjecthierarchy.createFile(parent_dir, list of files to create) : create a liste of files in the parent_dir provided
    setDataProjecthierarchy.createNestedDir(parent_dir, list of folder at depper level) : create a list of directories given in the parameter as a list of strings in the parent_dir provided
    setDataProjecthierarchy.gitCommit() : commits and pushes the repository 