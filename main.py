from pathlib import Path
from git import Repo
import nbformat as nbf
from re import search

class setDataProjecthierarchy:
    def createSingleDir(directoryname:list):
        # path 
        #path = Path.cwd().joinpath(Path(directoryname))
        #create directoiry given as parameter
        for dir_name in directoryname:
            Path(dir_name).mkdir(exist_ok=True)
            print(f"Directory {dir_name} created successfully")
        

    def createNestedDir(parent_dir, directoryname:list):
        #creates nested directory
        for dir_name in directoryname:
            # Path 
            path = Path(parent_dir).joinpath(Path(dir_name))
            #print(path)
            Path(path).mkdir(exist_ok=True)

    def createFile(parent_dir, filenames:list):
        #creates nested directory
        for filename in filenames:
            filepath = Path(parent_dir).joinpath(filename) 
            print(filepath)
            if search('ipynb', str(filepath).lower()):
                # title cell
                nb = nbf.v4.new_notebook()
                title = """\
                # Generated notebook."""
                nb['cells'] = [nbf.v4.new_markdown_cell(title)]
                with open(filepath, 'w') as f:
                    nbf.write(nb, f)
            else:
                #create file
                filepath.touch(exist_ok=True)

            

    def gitCommit():
        #executes git commit and pushes
        repo = Repo(Path.cwd())
        status = repo.git.status()
        print(status)
        files = repo.git.diff(None, name_only=True)
        # for f in files.split('\n'):
        repo.git.add('.')

        repo.git.commit('-m', 'Resolve directories creation', author='maskebe@gmail.com') 
        origin = repo.remote(name='origin')
        origin.push()      

    def cleanfiles(path, filename):
    #clean files and folders provided
        Path.unlink(path.joinpath(filename))

if __name__ == "__main__":
    setDataProjecthierarchy.createSingleDir(('data', 'docs', 'models', 'notebooks', 'reports', 'src'))
    setDataProjecthierarchy.createFile(Path.cwd(), ('LICENCE', 'Makefile', 'README.md', 'requirements.txt'))
    setDataProjecthierarchy.createNestedDir(Path.cwd().joinpath('data'), ('cleaned', 'processed', 'raw'))
    setDataProjecthierarchy.createFile(Path.cwd().joinpath('src'), ('utils.py', 'process.py', 'train.py'))
    setDataProjecthierarchy.createFile(Path.cwd().joinpath('notebooks'), ('main.ipynb', ''))
    setDataProjecthierarchy.gitCommit()